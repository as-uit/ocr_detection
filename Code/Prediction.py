from Code.PreProccess import PreProccess
from Code.Training import Training
import numpy as np

#Predict new data based on model
#Author: Luu Thanh Son - 14520772

class Prediction:
    def __init__(self, new_data, model):
        self.new_data = new_data
        self.model = model
    def predict(self):
        if (not(self.new_data.shape == [1, 28, 28, 1])):
            self.new_data = self.new_data.reshape([1, 28, 28, 1])
        predictArray = np.round(self.model.predict(self.new_data))
        return np.argmax(predictArray)
