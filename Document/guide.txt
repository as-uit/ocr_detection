# Chuong trinh co 3 file chinh de thuc thi
-----------------
File mnist_reader.py:  File nay dung de lay du lieu MNIST tu website cua Yan Le Cunn
    Input: No
    Output: cac file MNIST data training
-----------------
File PreProcess: Tien xu ly tap data training. Doc va phan tich du lieu cua data_training
    Input: No
    Output: tap training X va Y
------------------
File Training: Xay dung mo hinh mang neural de training
    Input: Tap training gom X va Y
    Output: mo hinh training
------------------
File Prediction: Thuc hien du doan du lieu moi dua tren mo hinh training co san. (Goi class Prediction khi chay la OK)
    Input: Tap du lieu moi (Buc anh da duoc so hoa, khong can reshape lai)
    Output: Ket qua du doan
------------------
File Main.py
    Thuc thi xu ly chinh cua chuong trinh trong file nay
    Neu muon du doan buc hinh moi chi can goi class Prediction len va goi ham predict de thuc thi.
    VD: Xem file Main.py
------------------
File ImgPreprocessing.py
    Xu ly extract thong tin tu buc anh Sodoku.
    Input: Buc anh sodoku dau vao.
    Output: Cac chu so da duoc detect. 
Luu y: cac chu so da extract se duoc luu trong folder Image/extracted.

## Cau truc thu muc ##
1. Code: Chua toan bo Source code cua chuong trinh
2. Document: Chua tai lieu va bao cao do an
3. Image: Luu tru hinh anh
	3.1 input: Buc anh dau vao
	3.2 extracted: Chua cac anh da extract. Tat ca cac hinh trong folder nay deu ko dc commit len git (da ignore)
4. Model: Luu tru model da training (cung dc ignore khi push len git)
5. training_data: Luu tru du lieu training.
6. *file app.py: day la file thuc thi chinh cua chuong trinh. 