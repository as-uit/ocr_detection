from Code.Prediction import Prediction
from Code.PreProccess import PreProccess
from Code.Training import Training
from Code.Image import Image
import numpy as np
import cv2

mnist = PreProccess()
(trainX, trainY) = mnist.getTrainData()
train = Training(trainX, trainY)
model = train.training()

print('====== TEST CASR 1 ======')
result = []
img = Image('Image/input/Test3.jpg')
arr_img = img.getNumberBox()
for i in range(0, len(arr_img)):
    if arr_img[i] == []:
        result.append('0')
    else:
        pre = Prediction(arr_img[i], model)
        result.append(pre.predict())

result = np.reshape(result,(9,9))
print(result)

print('====== TEST CASR 2 ======')
result = []
img = Image('Image/input/Test3.jpg')
arr_img = img.getNumberBox()
for i in range(0, len(arr_img)):
    if arr_img[i] == []:
        result.append('0')
    else:
        pre = Prediction(arr_img[i], model)
        result.append(pre.predict())

result = np.reshape(result,(9,9))
print(result)

print('====== TEST CASR 3 ======')
result = []
img = Image('Image/input/Test3.jpg')
arr_img = img.getNumberBox()
for i in range(0, len(arr_img)):
    if arr_img[i] == []:
        result.append('0')
    else:
        pre = Prediction(arr_img[i], model)
        result.append(pre.predict())
result = np.reshape(result,(9,9))
print(result)

print('====== TEST CASR 4 ======')
result = []
img = Image('Image/input/Test4.jpg')
arr_img = img.getNumberBox()
for i in range(0, len(arr_img)):
    if arr_img[i] == []:
        result.append('0')
    else:
        pre = Prediction(arr_img[i], model)
        result.append(pre.predict())
result = np.reshape(result,(9,9))
print(result)

print('====== TEST CASR 5 ======')
result = []
img = Image('Image/input/Test5.jpg')
arr_img = img.getNumberBox()
for i in range(0, len(arr_img)):
    if arr_img[i] == []:
        result.append('0')
    else:
        pre = Prediction(arr_img[i], model)
        result.append(pre.predict())

result = np.reshape(result,(9,9))
print(result)
